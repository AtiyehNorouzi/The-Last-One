﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageManager : MonoBehaviour
{
    [SerializeField]
    private GameObject stagesParent;
    private static StageManager _instance;
    public static StageManager instance
    {
        get
        {
            if (_instance != null)
                _instance = GameObject.FindObjectOfType<StageManager>();
            return _instance;
        }
    }
    void Awake()
    {
        _instance = this;
    } 
    public void MoveStage(int stageNumber , float ratio)
    {
        GameObject stage = stagesParent.transform.GetChild(stageNumber).gameObject;
        StartCoroutine(OnMoveStage(stage, stage.transform.position.y * ratio));
    }
    IEnumerator OnMoveStage(GameObject obj , float target)
    {
        yield return new WaitForSeconds(0.6f);
        while (Mathf.Abs(obj.transform.position.y - target) > 0.4f)
        {
            obj.transform.position = Vector3.right * obj.transform.position.x + Vector3.up * Mathf.Lerp(obj.transform.position.y, target, Time.deltaTime);
            yield return null;
        }
    }
   
}
