﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class Player : NetworkBehaviour
{
    enum jumpState { FALL, JUMP };
    jumpState jState = jumpState.JUMP;
    [SerializeField]
    private Sprite playerSprite;
    [SerializeField]
    private Rigidbody2D rb;
    [SerializeField]
    private SpriteRenderer spriteRenderer;
    private CameraMovement cameraMove;
    private Rigidbody2D rg;
    float moveHorizontal;

    const float moveValue =5;
    const float jumpValue = 50;
    const float redPlayerLayer = 8;
    const float bluePlayerLayer = 9;
    const float redPortalLayer = 11;
    const float bluePortalLayer = 10;
    const float whitePortalLayer = 12;
    RaycastHit2D hit;
    public override void OnStartLocalPlayer()
    {
        if (isServer)
        {
            spriteRenderer.sprite = playerSprite;
            gameObject.layer = LayerMask.NameToLayer("BluePlayer");
        }
    }
    void Start()
    {
        if (isClient && !isServer)
        {
            if(!isLocalPlayer)
            {
                spriteRenderer.sprite = playerSprite;
                gameObject.layer = LayerMask.NameToLayer("BluePlayer");
            }
        }
        if (!isLocalPlayer)
        {
           this.enabled = false;
        }
        cameraMove = GameObject.FindObjectOfType<CameraMovement>();
        rg = GetComponent<Rigidbody2D>();
        cameraMove.AssignPlayers(gameObject);
    }
   [ClientCallback]
    void Update ()
    {
        if (!isLocalPlayer)
            return;

        moveHorizontal = Input.GetAxis("Horizontal");
        rb.velocity = new Vector3(moveHorizontal * moveValue, rb.velocity.y);
        hit = Physics2D.Raycast(transform.position, Vector3.down, 0.5f, LayerMask.GetMask(gameObject.layer == 8 ? "RedPortal" : "BluePortal" , "WhitePortal"));
        if (hit.collider != null)
        {
              GetComponent<Collider2D>().isTrigger = true;
           // RpcSetTrigger(true);
        }
        hit = Physics2D.Raycast(transform.position, Vector3.up, 0.3f, LayerMask.GetMask(gameObject.layer == 8 ? "RedPortal" : "BluePortal" , "WhitePortal"));
        if (hit.collider != null)
        {
              GetComponent<Collider2D>().isTrigger = true;
           // RpcSetTrigger(true);
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            if (Mathf.Abs(rg.velocity.y) < 0.1f && jState == jumpState.JUMP)
            {
                jState = jumpState.JUMP;
                rb.AddForce(new Vector3(0, -1 * Physics2D.gravity.y * jumpValue, 0));
            }
        }

        if (rg.velocity.y <= 0 && jState == jumpState.FALL)
        {
            jState = jumpState.JUMP;
        }
    }
    [ClientRpc]
    void RpcSetTrigger(bool enable)
    {
        GetComponent<Collider2D>().isTrigger = enable;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "ground")
        {
            RpcSetTrigger(false);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Portal")
        {
            Debug.Log("In Trigger");
            if (gameObject.layer == redPlayerLayer && collision.gameObject.layer == redPortalLayer ||
                (gameObject.layer == bluePlayerLayer && collision.gameObject.layer == bluePortalLayer) || collision.gameObject.layer == whitePortalLayer)
            {
                if(Mathf.Abs(rg.velocity.y) > 5)
                {
                    rg.velocity = new Vector3(rg.velocity.x, Mathf.Sign(rg.velocity.y) * 5);
                }
                spriteRenderer.flipY = !spriteRenderer.flipY;
                if (isLocalPlayer)
                { 
                    Physics2D.gravity = new Vector3(0, -1 * Physics2D.gravity.y, 0);
                    RpcSetTrigger(false);
                }
            }
        }
    }

}