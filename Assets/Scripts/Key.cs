﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour
{
    public int stageNumToUnlock;
    public float moveRateRatio;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        transform.position += Vector3.right * GetComponent<SpriteRenderer>().bounds.size.x;
        StageManager.instance.MoveStage(0, moveRateRatio);
    }
}
